# -*- coding: utf-8 -*-
'''
Created on Nov 12, 2013

@author: Lê Ngọc Minh
'''
from composes.utils import io_utils
from composes.similarity.cos import CosSimilarity
from Queue import PriorityQueue
from optparse import OptionParser
import fileinput
import logging
import sys
import numpy
import os
import pickle


__log = logging.getLogger('examine')


def find_representatives_by_argsort(space, num_repr=15):
    __log.info("Computing representatives...")
    (num_row, num_col) = space.cooccurrence_matrix.shape
    sorted_indices = numpy.argsort(space.cooccurrence_matrix.mat, 0)
    __log.info("Finished sorting.")
    representatives = {}
    for col in range(num_col):
        reps = []
        for i in range(num_row-num_repr, num_row):
            row = sorted_indices[i, col]
            item = (space.id2row[row], space.cooccurrence_matrix[row, col])
            reps.append(item)
        representatives[col] = reps
    __log.info("Finished finding representatives.")
    return representatives
    
    
def find_representatives(space, num_repr=15):
    __log.info("Computing representatives...")
    representatives = {}
    (num_row, num_col) = space.cooccurrence_matrix.shape
    __log.debug("Matrix size: %dx%d" %(num_row, num_col))
    for col in range(num_col):
        queue = PriorityQueue(num_repr+1)
        for row in xrange(num_row):
            weight = space.cooccurrence_matrix[row,col]
            queue.put((weight, row))
            if queue.full(): queue.get()
            
        reps = []
        while not queue.empty():
            item = queue.get()
            item = (space.id2row[item[1]], item[0])
            reps.append(item)
        representatives[col] = reps
        
        if (col+1) % 10 == 0:
            __log.debug("Processing... %d" %(col+1))
    __log.info("Finished finding representatives.")
    return representatives


def print_dim_representatives(dims, f=sys.stdout):
    for (index, dim) in enumerate(dims):
        if index > 0: f.write(", ")
        f.write("%s=%.2f" %(dim[0], dim[1]))
        

def print_dimensions(space, representatives, word):
    row = space.get_row(word)
    indices = sorted([col for col in range(row.shape[1])], 
                     key=lambda col: row[0,col], reverse=True)[:5]
    for index in indices:
        weight = row[0,index]
        reps = representatives[index]
        sys.stdout.write('Dimension %d (%f): ' %(index, weight))
        print_dim_representatives(reps)
        print


def print_representatives(representatives):
    print '=== Representatives ==='
    for i in representatives:
        sys.stdout.write("Dimension %d: " %i) 
        print_dim_representatives(representatives[i])
        print


def print_statistics(space):
    __log.info("Computing statistics...")
    eps = 1E-6
    matrix = space.cooccurrence_matrix.mat
    nonzeros = matrix >= eps
    nonzero_count_cols = numpy.squeeze(numpy.array(numpy.sum(nonzeros, 0)))
    nonzero_count_rows = numpy.squeeze(numpy.array(numpy.sum(nonzeros, 1)))
    nonzero_count_matrix = numpy.sum(nonzero_count_rows)
    (num_row, num_col) = matrix.shape
    sparsity = 1-float(nonzero_count_matrix)/(num_row*num_col)
    print "Dimensions: %dx%d" %(num_row, num_col)
    print "Sparsity: %.2f%%" %(sparsity*100)
    print "avg(#dimensions per word): %.2f" %nonzero_count_rows.mean()
    print "std(#dimensions per word): %.2f" %nonzero_count_rows.std()
    print "avg(#words per dimension): %.2f" %nonzero_count_cols.mean()
    print "std(#words per dimension): %.2f" %nonzero_count_cols.std()
#    print repr(nonzero_count_rows)
#    print repr(numpy.bincount(nonzero_count_rows))
    print "Distribution of #dimensions per word:" 
    print numpy.bincount(nonzero_count_rows)
    print "Distribution of (#words per dimension)/100 (first 1000 counts):" 
    print numpy.bincount(nonzero_count_cols/100)[:1000]
    sys.stdout.write("First 500 words with least number of dimensions: ")
    sorted_rows = numpy.argsort(nonzero_count_rows)
    for i in range(500):
        if i > 0: sys.stdout.write(", ")
        sys.stdout.write(space.id2row[sorted_rows[i]])
    print
    

def examine_all(space, words, space_path=None):
    print_statistics(space)
    if space_path:
        dim_path = space_path + '.dim' 
        if os.path.exists(dim_path):
            with open(dim_path) as f:
                representatives = pickle.load(f)
        else:
            representatives = find_representatives_by_argsort(space)
            with open(dim_path,'w') as f:
                pickle.dump(representatives, f, 0)
    else:
        representatives = find_representatives_by_argsort(space)
    print_representatives(representatives)
    for word in words:
        examine(space, word, representatives)
        

def print_weights(weights):
    sys.stdout.write('Weights: ')
    for col in range(weights.shape[1]):
        if col > 0: sys.stdout.write('\t')
        sys.stdout.write("%5.2f" %weights[0, col])
        if (col+1) % 15 == 0: print
    print
    

def examine(space, word, representatives):
    try:
        word = word.strip()
        print '=== %s ===' %word
        print_weights(space.get_row(word)) 
        neighbours = space.get_neighbours(word, 10, CosSimilarity())
        print 'Neighbors: %s\t%s' %(word, str(neighbours))
        print_dimensions(space, representatives, word)
    except KeyError:
        print "Word not found: %s" %word


if __name__ == '__main__':
#    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
#    logFormatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    consoleHandler = logging.StreamHandler()
#    consoleHandler.setFormatter(logFormatter)
    __log.addHandler(consoleHandler)
    __log.setLevel(logging.DEBUG)
    
    parser = OptionParser(usage="usage: %prog options [word-listing-files]")
    parser.add_option("-s", "--space", metavar="FILE",
                      help="read the examined space from FILE")
    (options, args) = parser.parse_args()
    __log.info("Read from space %s" %options.space)
    __log.info("Word lists: %s" %str(args))
    __log.info("Reading space...")
    space = io_utils.load(options.space)
    __log.info("Done.")
    examine_all(space, fileinput.input(args), options.space)