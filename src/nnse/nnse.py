# -*- coding: utf-8 -*-
'''
Created on Sep 13, 2013

@author: Lê Ngọc Minh
'''

from composes.matrix.sparse_matrix import SparseMatrix
from composes.transformation.dim_reduction.dimensionality_reduction import \
    DimensionalityReduction
from spams import trainDL, lasso
import numpy
import spams_wrap


class NNSE(DimensionalityReduction):
    """
    Performs non-negative sparse embedding on a matrix 
    """
    
    _name = "nnse"
    
    def __init__(self, reduced_dimension, lambda1):
        '''
        Constructor
        '''
        super(NNSE, self).__init__(reduced_dimension)
        self.lambda1 = lambda1
        
        
    def apply(self, matrix_):
        new_matrix = numpy.transpose(matrix_.mat) 
        new_matrix = numpy.asfortranarray(new_matrix, float)
        d, new_matrix = nnse_impl(new_matrix, lambda1=self.lambda1, 
                           K=self.reduced_dimension, return_lasso=True)
        d = None # release memory
        new_matrix = numpy.transpose(new_matrix)
        return SparseMatrix(new_matrix), None
        
    
def nnse_impl(X,return_lasso= False,model= None,lambda1= None,
         numThreads = -1,batchsize = -1,K= -1,
         iter=-1,t0=1e-5,clean=True,rho=1.0,modeParam=0,batch=False):
    """
        Perform non-negative sparse embedding technique as appeared in: 
        
        Murphy, B., Pratim, P., & Tom, T. (2012). Learning Effective and 
        Interpretable Semantic Models using Non-Negative Sparse Embedding. 
        In International Conference on Computational Linguistics.
        
        Non-Negative Sparse Embedding (NNSE) is a variation on Non-Negative 
        Sparse Coding (NNSC) that does not impose a constraint of non-negativity 
        on D.
        
        Potentially, n can be very large with this algorithm.

    Args:
        X: double m x n matrix   (input signals)
          m is the signal size
          n is the number of signals to decompose
        return_lasso: 
          if true the function will return a tuple of matrices.

    Kwargs:
        K: (number of required factors)
        lambda1: (parameter)
        iter: (number of iterations).  If a negative number 
          is provided it will perform the computation during the
          corresponding number of seconds. For instance iter=-5
          learns the dictionary during 5 seconds.
        batchsize: (optional, size of the minibatch, by default 
          512)
        modeParam: (optimization mode).
          1) if modeParam=0, the optimization uses the 
          parameter free strategy of the ICML paper
          2) if modeParam=1, the optimization uses the 
          parameters rho as in arXiv:0908.0050
          3) if modeParam=2, the optimization uses exponential 
          decay weights with updates of the form 
          A_{t} <- rho A_{t-1} + alpha_t alpha_t^T
        rho: (optional) tuning parameter (see paper
          arXiv:0908.0050)
        t0: (optional) tuning parameter (see paper 
          arXiv:0908.0050)
        clean: (optional, true by default. prunes automatically 
          the dictionary from unused elements).
        batch: (optional, false by default, use batch learning 
          instead of online learning)
        numThreads: (optional, number of threads for exploiting
          multi-core / multi-cpus. By default, it takes the value -1,
          which automatically selects all the available CPUs/cores).
        model: struct (optional) learned model for "retraining" the data.

    Returns:
        U: double m x p matrix   
        V: double p x n matrix   (optional)
        model: struct (optional) learned model to be used for 
          "retraining" the data.
          U = spams.nnsc(X,return_lasso = False,...)
          (U,V) = spams.nnsc(X,return_lasso = True,...)

    Authors:
    Julien MAIRAL, 2009 (spams, matlab interface and documentation)
    Jean-Paul CHIEZE 2011-2012 (python interface)
    Ngoc-Minh Le
    """

    if lambda1 == None:
        raise ValueError("nnse : lambda1 must be defined")
    U = trainDL(X,model = model,numThreads = numThreads,batchsize = batchsize,
                K = K,iter = iter, t0 = t0, clean = clean, rho = rho,verbose=False, 
                modeParam = modeParam,batch = batch, lambda1 = lambda1,
                mode = spams_wrap.PENALTY, posAlpha=True,posD=False,whiten=False)
    if not return_lasso:
        return U
    V = lasso(X, D = U,return_reg_path = False, numThreads = numThreads,
                  lambda1 = lambda1, pos=True, mode = spams_wrap.PENALTY)
    return (U, V)