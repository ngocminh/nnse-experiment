# -*- coding: utf-8 -*-
'''
Created on Feb 13, 2013

@author: Lê Ngọc Minh
'''
import logging


__log = logging.getLogger(__name__)


class Question:
    
    def __init__(self, modifier, head, correct_answer, foils):
        self.modifier = modifier
        self.head = head
        self.correct_answer = correct_answer
        self.foils = foils
        
        
    @property
    def question(self):
        return self.modifier + "_" + self.head
    
    
    @property
    def choices(self):
        choices = [self.modifier, self.head, self.correct_answer]
        choices.extend(self.foils)
        return choices
    
    
    def __str__(self):
        return "(%s %s: %s)" % (self.modifier, self.head, self.correct_answer)


def of(s):
    chunks = s.split("\t")
    for index, chunk in enumerate(chunks):
        chunks[index] = chunk.strip()
    if len(chunks) < 3:
        raise ValueError("At least 3 fields are required.")
    question = Question(chunks[0], chunks[1], chunks[2], chunks[3:])
    return question


def from_file(paths, supress_error=True):
    if isinstance(paths, basestring):
        paths = (paths, )
    questions = []
    for path in paths:
        with open(path) as f:
            for line in f:
                try:
                    questions.append(of(line))
                except ValueError as e:
                    if supress_error:
                        __log.error("Invalid question: " + line, exc_info=1)
                    else:
                        raise e
    return questions