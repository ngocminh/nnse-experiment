# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from collections import Counter
from glob import glob
from logging import getLogger
from random import random
import gzip
import locale
import re


_log = getLogger("ex42.ukwac")


class DependencyRelation:
    """
    Represent a relation between two DependencyTreeNode.
    """
    
    def __init__(self, head, depend, label=None):
        self.head = head
        self.depend = depend
        self.label = label
        
        
    def __str__(self):
        return "%s --> %s" %(self.head, self.depend)

    
class DependencyTreeNode:
    """
    Store various information (some is optional) of a node in a dependency tree.
    word: the word form
    pos: part-of-speech
    lemma: lemma form (optional)
    index: position of this word in sentence (0-based)
    relations: a (mutable) list of DependencyRelation. Notice that it store all
               the relations in which this node is head or dependent node.
    """
    
    def __init__(self, word, pos, lemma, index, id_=None, head_id=None, relations=None):
        self.word = word
        self.pos = pos
        self.lemma = lemma
        self.index = index
        self.id = id_
        self.head_id = head_id
        if relations is None:
            self.relations = []
        else:
            self.relations = relations


    def add_relation(self, relation):
        self.relations.append(relation)


    def find_parent(self):
        for relation in self.relations:
            if self is relation.depend:
                return relation.head
        return None
        
    
    def __str__(self):
        return "%s (lemma=%s, pos=%s, index=%d)" %(self.word, self.lemma, 
                                                   self.pos, self.index)


def lemma(node):
    return node.lemma


_text_tag_pattern = re.compile("^\s*</?text")


class SentenceIterator:
    """
    Turn a stream formated in UKWac format into a iterator of sentences.
    """
    
    def __init__(self, paths):
        """
        Files is a path or list/tuple of paths
        """
        if not paths:
            raise ValueError("Paths cannot be empty")
        if isinstance(paths, basestring):
            paths = [paths]
        self.paths = paths
        self.file = self.next_file()
        self.sentence = [] # reuse sentence list for better performance
    
    
    def __iter__(self):
        return self


    def next_file(self):
        if not self.paths:
            raise StopIteration()
        path = self.paths[0]
        self.paths = self.paths[1:]
        if path.endswith(".gz"):
            return gzip.open(path)
        else:
            return open(path, "r")

    
    def next(self):
        line = self.file.readline()
        while line:
            if line.startswith("<s>"):
                del self.sentence[:] # clear the list for reuse
                line = self.file.readline()
                while line and not line.startswith("</s>"):
                    # this shouldn't happen but... it does!
                    if _text_tag_pattern.match(line):
                        break
                    if line != "<s>":
                        self.sentence.append(line)
                    line = self.file.readline()
                else:
                    return "".join(self.sentence)
                # an error occur, skip the whole sentence and try the next
                if len(self.sentence) > 0:
                    _log.warn("Sentence is not terminated correctly, skipped: " +
                               "".join(self.sentence))
#                else:
#                    _log.warn("Empty sentence followed by <text> or </text>, skipped.")
                continue
            line = self.file.readline()
        self.file = self.next_file()
        return self.next()

        
    def __enter__(self):
        return self


    def __exit__(self, type_, value, traceback):
        if self.file:
            self.file.close()
        return False


def sentences(inp):
    """
    Iterate all sentences within a stream (see SentenceIterator).
    
    CAUTION! This method won't close the file until all of its content has been 
    read. Only use it when you want everything in a file.
    """
    with SentenceIterator(inp) as reader:
        for sentence in reader:
            yield sentence


def dependency_nodes(sentence, relation_parsed=True):
    """
    Return a list of DependenceNode object for given sentence string.
    """
    nodes = []
    for line in sentence.splitlines():
        line = line.strip()
        if len(line) <= 0:
            continue # fix some mistake in my data
        row = line.split('\t')
        if len(row) < 5:
            raise ValueError("Invalid node specification: " + line)
        node = DependencyTreeNode(row[0], row[2], row[1], 
                                  len(nodes), row[3], row[4])
        nodes.append(node)
    if relation_parsed:
        parse_relations(nodes)
    return nodes


def parse_relations(nodes):
    id2node = {'0':DependencyTreeNode('ROOT', 'ROOT', 'ROOT', -1)}
    for node in nodes:
        id2node[node.id] = node
    for node in nodes:
        head = id2node[node.head_id]
        relation = DependencyRelation(head, node)
        head.add_relation(relation)
        node.add_relation(relation)


def coocurrence_lemma(relation, row_node):
    """
    An extract_column callback for coocurence methods. This method return a 
    column id that corresponds to the lemma of neighbor node of the specified 
    row_node in the specified relation.
    """
    column_node = None
    if relation.head is row_node:
        column_node = relation.depend
    elif relation.depend is row_node:
        column_node = relation.head
    else:
        raise ValueError("Unrelated relation?")
    return column_node.lemma


def __increase_count(dict_, key):
    count = dict_.get(key, 0)
    count += 1
    dict_[key] = count


def dependency_coocurrences(paths, 
                           filter_row_node=lambda node: True, 
                           extract_row=lemma,
                           extract_column=coocurrence_lemma):
    """
    Retrieve all coocurence counts and return a dictionary that keys are 
    (row, column) and values are counts. This implementation store everything
    in memory so remember to provide it with a good filter callback.
    """
    coocurrences = {}
    for sentence in sentences(paths):
        for node in dependency_nodes(sentence):
            if not filter_row_node(node):
                break
            row = extract_row(node)
            for relation in node.relations:
                column = extract_column(relation, node)
                __increase_count(coocurrences, (row, column))
    return coocurrences


def node2str(node):
    return node.lemma + "-" + node.pos[0].lower()
    
    
def most_common(paths, n, 
                filter_node = lambda node: True, 
                convert_node = node2str):
    counter = Counter()
    for sentence in sentences(paths):
        for node in dependency_nodes(sentence):
            if not filter_node(node):
                continue
            counter[convert_node(node)] += 1
    return counter.most_common(n)


def __open_files_for_writing(n, output_path_pattern, gz):
    files = []
    for i in range(n):
        path = output_path_pattern %i
        if gz:
            files.append(gzip.open(path, "w"))
        else:
            files.append(open(path, "w"))
    return files


def __close_files(files):
    for i in range(len(files)):
        files[i].close()


def __glob_all(paths):
    if isinstance(paths, basestring):
        paths = (paths,)
    new_paths = []
    for path in paths:
        new_paths.extend(glob(path))
    return new_paths


def _roulette(n, min_priority, max_priority):
    roulette = []
    total = n * float(min_priority + max_priority) / 2
    prob = 0
    for i in range(n):
        roulette.append(prob)
        prob += (min_priority + (i+1) * (max_priority - min_priority) / n) / total
    return roulette


def __write_sentence(f, sentence):
    # no string format or concatenation here!
    f.write("<s>\n")
    f.write(sentence.strip())
    f.write("\n</s>\n")


def _roulette_binary_search(a, x, lo=0, hi=None):
    if hi is None:
        hi = len(a)
    while lo + 1 < hi:
        mid = (lo+hi)/2
        midval = a[mid]
        if midval <= x:
            lo = mid
        elif midval > x: 
            hi = mid
    return lo


def random_split(input_paths, output_path_pattern, n, 
                 min_priority, max_priority = 1, gzip = True):
    roulette = _roulette(n, min_priority, max_priority)
    files = __open_files_for_writing(n, output_path_pattern, gzip)
    counters = [0]*n
    total_counter = 0
    input_paths = __glob_all(input_paths)
    for sentence in sentences(input_paths):
        r = random()
        i = _roulette_binary_search(roulette, r)
        __write_sentence(files[i], sentence)
        counters[i] += 1
        total_counter += 1
        if total_counter % 100000 == 0:
            _log.debug("Total processed sentences: " +  
                       locale.format("%d", total_counter, grouping=True))
    __close_files(files)
    return counters

'''
if __name__ == "__main__":
    random_split("test-data/split-linear-small/bnc-0-*.xml.gz", 
                 "test-data/split-equal-small/bnc-0-%02d.xml.gz", 
                 40, min_priority=1, max_priority=1, gzip=True);
'''

def is_noun(node):
    '''
    To fix an error in UkWaC and maybe other corpora that
    tags "NN" for percentage sign (%).
    '''
    return node.pos.startswith("N") and node.word != "%"


