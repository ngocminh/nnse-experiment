EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` <config-file>"
  exit $E_BADARGS
fi

CORPUS_PATHS=`grep corpora $1 | grep -v "#" | cut -d "=" -f2 | sed 's/,/ /g'`
echo "Listing of all files: "
ls $CORPUS_PATHS
echo -n "Total: "
ls $CORPUS_PATHS | wc -l
