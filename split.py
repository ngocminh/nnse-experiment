import os
from ex42 import ukwac
output_dir = 'data/split-new'
os.mkdir(output_dir)
ukwac.random_split(['data/*.xml.gz'], output_dir + '/split-%03d.xml.gz', 
                   100, 0.4, 1, True)
