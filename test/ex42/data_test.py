# -*- coding: utf-8 -*-
'''
Created on Mar 12, 2013

@author: Lê Ngọc Minh
'''
import unittest
from ex42.data import merge, sort_and_write_all, write_counter
from collections import Counter


class Test(unittest.TestCase):


    def test_merge(self):
        merge("target/merge", ["test-data/counts1", "test-data/counts2"])
        with open("test-data/counts3", "r") as f:
            expected_result = f.read().strip()
        with open("target/merge", "r") as f:
            self.assertEquals(expected_result, f.read().strip())


    def test_merge_three_way(self):
        merge("target/merge", ["test-data/counts1", "test-data/counts2", 
                               "test-data/counts4"])
        with open("test-data/counts5", "r") as f:
            expected_result = f.read().strip()
        with open("target/merge", "r") as f:
            self.assertEquals(expected_result, f.read().strip())


    def test_merge_single_word_randomized(self):
        pass
    
    
    def test_sort_and_write_simple(self):
        counter = Counter({"a": 1000, "x": 100, "bc": 102})
        sort_and_write_all(counter, "target/sorted-simple")
        expected_result = 'a\t1000\nbc\t102\nx\t100'
        with open("target/sorted-simple", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_sort_ordering(self):
        counter = Counter({("people/"): 12, 
                           ("people"): 20})
        sort_and_write_all(counter, "target/sorted-ordering")
        expected_result = ('people/\t12\n'
                           'people\t20') # it's a strang ordering
        with open("target/sorted-ordering", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_sort_ordering2(self):
        counter = Counter({("people/"): 32, 
                           ("people"): 20})
        sort_and_write_all(counter, "target/sorted-ordering2")
        expected_result = ('people/\t32\n'
                           'people\t20') # it's a strang ordering
        with open("target/sorted-ordering2", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_sort_and_write_tuples(self):
        counter = Counter({("a", "b"): 1000, 
                           ("x", "y"): 100, 
                           ("bc", "a"): 102})
        sort_and_write_all(counter, "target/sorted-tuples")
        expected_result = ('a\tb\t1000\n'
                           'bc\ta\t102\n'
                           'x\ty\t100')
        with open("target/sorted-tuples", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_sort_and_write_then_merge(self):
        a = Counter({"a":2,"b":12,"c":21,"e":8})
        b = Counter({      "b":1, "c":10,"d":5})
        c = Counter({"a":2,       "c":21,      "f":8})
        d = Counter(a); d.update(b); d.update(c)
        sort_and_write_all(a, "target/sorted-a")
        sort_and_write_all(b, "target/sorted-b")
        sort_and_write_all(c, "target/sorted-c")
        merge("target/sorted-d", ["target/sorted-a", "target/sorted-b", "target/sorted-c"])
        expected_result = "\n".join([key + "\t" + str(d[key]) for key in sorted(d.keys())])
        with open("target/sorted-d", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_write_counter_locale(self):
        counter = Counter({"people/": 12, 
                           "people": 20})
        write_counter(counter, "target/sorted-locale")
        expected_result = ('people\t20\n'
                           'people/\t12')
        with open("target/sorted-locale", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_write_counter_locale2(self):
        counter = Counter({("people/", "a"): 12, 
                           ("people", "b"): 20})
        write_counter(counter, "target/sorted-locale2")
        expected_result = ('people\tb\t20\n'
                           'people/\ta\t12')
        with open("target/sorted-locale2", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    
    
    def test_write_counter_then_merge(self):
        a = Counter({"a":2,"b":12,"c":21,"e":8})
        b = Counter({      "b":1, "c":10,"d":5})
        c = Counter({"a":2,       "c":21,      "f":8})
        d = Counter(a); d.update(b); d.update(c)
        write_counter(a, "target/sorted-a")
        write_counter(b, "target/sorted-b")
        write_counter(c, "target/sorted-c")
        merge("target/sorted-d", ["target/sorted-a", "target/sorted-b", "target/sorted-c"])
        expected_result = "\n".join([key + "\t" + str(d[key]) for key in sorted(d.keys())])
        with open("target/sorted-d", "r") as f:
            self.assertEquals(expected_result, f.read().strip())
    

# No longer needed
'''
    def test_update_normal_counter_async(self):
        big_counter = Counter()
        for i in xrange(1000000):
            big_counter[i] += 1
        async_counter = AsynchronousCounter(Counter())
        start = time.clock()
        for i in range(10):
            async_counter.update_async(big_counter)
        elapsed = (time.clock() - start) # seconds?
        async_counter.wait()
        for t in async_counter.most_common(3):
            self.assertEqual(10, t[1])
        self.assertLess(elapsed, 1)
'''
# Tree counter is no longer used
'''
    def test_update_tree_counter_async(self):
        big_counter = Counter()
        for i in xrange(1000000):
            big_counter[i] += 1
        async_counter = AsynchronousCounter(TreeCounter())
        start = time.clock()
        for i in range(10):
            async_counter.update_async(big_counter)
        elapsed = (time.clock() - start) # seconds?
        async_counter.wait()
        for t in async_counter.most_common(3):
            self.assertEqual(10, t[1])
        self.assertLess(elapsed, 1)

    def test_update_tree_counter(self):
        big_counter = Counter()
        for i in xrange(50000):
            big_counter[i] += 1
        tree_counter = TreeCounter()
        start = time.clock()
        for i in range(10):
            tree_counter.update(big_counter)
        elapsed = (time.clock() - start) # seconds?
        print "Time: %f" %elapsed
        for t in tree_counter.most_common(3):
            self.assertEqual(10, t[1])


    def test_count_tree_counter(self):
        tree_counter = TreeCounter()
        start = time.clock()
        for n in range(10):
            for i in xrange(50000):
                tree_counter[i] += 1
        elapsed = (time.clock() - start) # seconds?
        print "Time: %f" %elapsed
        for t in tree_counter.most_common(3):
            self.assertEqual(10, t[1])
'''
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_update_async']
    unittest.main()