# -*- coding: utf-8 -*-
'''
Created on Nov 16, 2013

@author: Lê Ngọc Minh
'''
from composes.utils import io_utils
from nnse.nnse import nnse_impl, NNSE
import numpy as np
import spams
import unittest


class Test(unittest.TestCase):


    def test_simple(self):
        x = np.array([[3.0, 2.0, 1.0, 0.0, 6.0],
                      [3,1,1,0,4],
                      [0,4,0,0,4],
                      [18,9,5,3,10],
                      [1,0,0,5,4],
                      [1,0,2,9,4]])
        x = np.transpose(x)
        (d, a) = nnse_impl(x, lambda1=0.1, K=3, return_lasso=True)
        a = a.todense()
        print str(a)
        expected = np.array([[4.48082582,2.16307536,5.23676233, 0, 1.45564183, 0 ],
                             [0.23544483, 0.05134124, 0, 0, 5.67333582, 9.98004519],
                             [2.98298473, 3.22921245, 0, 23.11338885, 0, 0]])
        self.assertTrue(np.allclose(expected, a), 
                        'Expected \n%s\nbut was\n%s' %(str(expected), str(a)))


    def test_negative(self):
        x = np.array([[3.0, 2.0, 1.0, 0.0, 6.0],
                      [-3,-1,-1,0,-4],
                      [0,4,0,0,4],
                      [18,9,5,3,10],
                      [1,0,0,5,4],
                      [1,0,2,9,4]])
        x = np.transpose(x)
        (d, a) = nnse_impl(x, lambda1=0.1, K=3, return_lasso=True)
        a = a.todense()
        print 'a=', a, '\nd=', d
        self.assertTrue(np.all(a>=0))
        self.assertFalse(np.all(d>=0))
        

    def test_negative_nnsc(self):
        # 5x6 matrix
        x = np.array([[3.0, 2.0, 1.0, 0.0, 6.0],
                      [-3,-1,-1,0,-4],
                      [0,4,0,0,4],
                      [18,9,5,3,10],
                      [1,0,0,5,4],
                      [1,0,2,9,4]])
        x = np.transpose(x)
        (d, a) = spams.nnsc(x, lambda1=0.1, K=3, return_lasso=True)
        a = a.todense()
        print 'a=', a, '\nd=', d
        self.assertTrue(np.all(d>=0))

        
    def test_transformation(self):
        space = io_utils.load('test-data/processed-domain-space.pkl') 
        space = space.apply(NNSE(5, 0.01))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_simple']
    unittest.main()