My experiment with Non-Negative Sparse Embedding [1]. I tried Turney's experiment in [2], section 4.2, domain space, multiplicative and additive composition. NNSE resulted in improved results with proper parameter configurations.

[1] Murphy, B., Pratim, P., & Tom, T. (2012). Learning Effective and Interpretable Semantic Models using Non-Negative Sparse Embedding. In International Conference on Computational Linguistics.

[2] Turney, P. D. (2012). Domain and Function : A Dual-Space Model of Semantic Relations and Compositions. Journal of artificial intelligence research, 44, 533–585.
